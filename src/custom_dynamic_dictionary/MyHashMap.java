package custom_dynamic_dictionary;

import java.util.Arrays;

public class MyHashMap<K, V> {

	private int size = 0;
	private int capacity = 1 << 4;
	private Entry<K, V>[] buckets;

	public int getSize() {
		return size;
	}

	public MyHashMap() {
		buckets = new Entry[this.capacity];
	}

	public MyHashMap(int capacity) {
		buckets = new Entry[this.capacity];
	}

	@Override
	public String toString() {
		return "MyHashMap [size=" + size + ", capacity=" + capacity + ", buckets=" + Arrays.toString(buckets) + "]";
	}

	void put(K key, V value) {

		Entry<K, V> entry = new Entry<K, V>(key, value);

		int bucketIndex = Math.abs(key.hashCode() % capacity);
		Entry<K, V> bucket = buckets[bucketIndex];

		if (bucket == null) {
			buckets[bucketIndex] = entry;
			size++;

		} else {

			while (bucket.next != null) {

				if (bucket.k.equals(key)) {
					bucket.v = value;
					return;
				}

				bucket = bucket.next;
			}

			if (bucket.k.equals(key)) {
				bucket.v = value;
			} else {
				bucket.next = entry;
				size++;
			}
		}
	}

	V get(K key) {
		int index = Math.abs(key.hashCode() % capacity);
		Entry<K, V> entry = buckets[index];

		while (entry != null) {
			if (entry.k.equals(key)) {
				return entry.v;
			}
			entry = entry.next;
		}

		return null;
	}
	

	class Entry<K, V> {
		final K k;
		V v;
		Entry<K, V> next;

		public Entry(K k, V v) {
			this.k = k;
			this.v = v;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getEnclosingInstance().hashCode();
			result = prime * result + ((k == null) ? 0 : k.hashCode());
			result = prime * result + ((next == null) ? 0 : next.hashCode());
			result = prime * result + ((v == null) ? 0 : v.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			
			if(!(obj instanceof MyHashMap.Entry)) {
				throw new ClassCastException("No se puede hacer el casting");
			}
			
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Entry<?, ?> other = (MyHashMap.Entry)obj;
			if (!getEnclosingInstance().equals(other.getEnclosingInstance()))
				return false;
			if (k == null) {
				if (other.k != null)
					return false;
			} else if (!k.equals(other.k))
				return false;
			if (next == null) {
				if (other.next != null)
					return false;
			} else if (!next.equals(other.next))
				return false;
			if (v == null) {
				if (other.v != null)
					return false;
			} else if (!v.equals(other.v))
				return false;
			return true;
		}

		private <K,V> MyHashMap<K, V> getEnclosingInstance() throws ClassCastException {
			return (MyHashMap<K, V>) MyHashMap.this;
		}

		@Override
		public String toString() {
			return "Entry [k=" + k + ", v=" + v + ", next=" + next + "]";
		}
	}
}
