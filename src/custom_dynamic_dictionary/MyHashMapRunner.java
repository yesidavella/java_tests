package custom_dynamic_dictionary;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Test;

public class MyHashMapRunner {

	public static void main(String[] args) {
		MyHashMap<String, String> myHashMap = new MyHashMap<String, String>();

		myHashMap.put("YesidKey", "Yesid Value");
		myHashMap.put("YesidKey2", "Yesid Value2");
		myHashMap.put("YesidKey3", "Yesid Value3");
		myHashMap.put("YesidKey4", "Yesid Value4");
		myHashMap.put("YesidKey5", "Yesid Value5");
		myHashMap.put("YesidKey6", "Yesid Value6");
		myHashMap.put("YesidKey7", "Yesid Value7");
		myHashMap.put("YesidKey8", "Yesid Value8");
		myHashMap.put("AndresdKey", "Andres Value");
		myHashMap.put("AndresdKey2", "Andres Value2");
		myHashMap.put("AndresdKey3", "Andres Value3");
		myHashMap.put("AndresdKey4", "Andres Value4");
		myHashMap.put("AndresdKey5", "Andres Value5");
		myHashMap.put("AndresdKey6", "Andres Value6");
		myHashMap.put("AndresdKey7", "Andres Value7");
		myHashMap.put("AndresdKey8", "Andres Value8");

		System.out.println("myHashMap value: " + myHashMap + myHashMap.get("YesidKey2"));

		System.out.println("Returned: " + myHashMap.get("AndresdKey8"));

		MyHashMap<String, Integer> myIntHashMap = new MyHashMap<String, Integer>();
		myIntHashMap.put("YesidKey", 2);
		myIntHashMap.put("YesidKey2", new Integer(4));
		
		System.out.println("Returned from IntHashMap: " + myIntHashMap.get("YesidKey"));
	}
	
	@Test
	public void testMyMap() {
		MyHashMap<String, String> myHashMap = new MyHashMap<String, String>();

		myHashMap.put("YesidKey", "Yesid Value");
		myHashMap.put("YesidKey2", "Yesid Value2");
		myHashMap.put("YesidKey3", "Yesid Value3");
		
		assertTrue(myHashMap.get("YesidKey").equals("Yesid Value"), "Quedo mal hecho el hp hashMap!!");
		assertEquals(3, myHashMap.getSize(), "No esta operando bien sobre el getSize()");
	}
}
