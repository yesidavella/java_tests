package travel_fus_test;

class Node {
    private Node leftChild, rightChild;
    
    public Node(Node leftChild, Node rightChild) {
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }
    
    public Node getLeftChild() {
        return this.leftChild;
    }
    
    public Node getRightChild() {
        return this.rightChild;
    }

    public int height() {
    	
    	int distance = 0;
    	
    	Node rightNode = this.getRightChild();
    	Node leftNode = this.getLeftChild();
    	
    	if(rightNode != null || leftNode != null) {
    		distance ++;
    	}
    	
    	int rightDistance = ( rightNode != null ) ? rightNode.height():0;
    	int leftDistance = ( leftNode != null ) ? leftNode.height():0;
    	
    	distance += Math.max(rightDistance, leftDistance);
    	
    	return distance;
        
    }

    public static void main(String[] args) {
        Node leaf1 = new Node(null, null);
        Node leaf2 = new Node(null, null);
        Node node = new Node(leaf1, null);
        Node root = new Node(node, leaf2);

        System.out.println(root.height());
    }
}
