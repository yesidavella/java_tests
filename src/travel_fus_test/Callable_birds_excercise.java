package travel_fus_test;

import java.util.concurrent.Callable;

interface Bird {
	Egg lay();
}

class Chicken implements Bird {
	
	public Chicken() {
	}

	public static void main(String[] args) throws Exception {
		Chicken chicken = new Chicken();
		System.out.println(chicken instanceof Bird);
	}

	@Override
	public Egg lay() {
		return new Egg((Callable<Bird>) this);
	}
}

class Egg {

	Callable<Bird> bird;

	public Egg(Callable<Bird> createBird) {
		this.bird = createBird;
	}

	public Bird hatch() throws Exception {
		return new Chicken();
	}
}
