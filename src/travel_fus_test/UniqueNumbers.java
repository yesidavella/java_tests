package travel_fus_test;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;

public class UniqueNumbers {

	public static Collection<Integer> findUniqueNumbers(Collection<Integer> numbers) {
		
		HashMap<Integer, Integer> map = new HashMap<>(); 
		
		for (int number : numbers) {
			
			if(map.containsKey(number)) {
				map.replace(number, map.get(number)+1);
			}else {
				map.put(number, 1);
			}
		}
		
		List<Integer> finalList = new ArrayList<>();
		
		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			
			if(entry.getValue() == 1) {
				finalList.add(entry.getKey());
			}
		}
		
		return finalList;
	}

	public static void main(String[] args) {
		Collection<Integer> numbers = Arrays.asList(1, 2, 1, 3);
		for (int number : findUniqueNumbers(numbers))
			System.out.println(number);
	}
}
