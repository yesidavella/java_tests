package codeexercises.com;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.LinkedList;

import org.junit.Test;

public class FreeTests {

	public String balancedBrackets(String s) {
		
		int lenght = s.length();
		LinkedList<Character> stack = new LinkedList<Character>();

		if (lenght % 2 != 0) {
			return "NO";
		}

		for (int i = 0; i < lenght; i++) {

			char pivot = s.charAt(i);

			if (pivot == '(' || pivot == '{' || pivot == '[') {
				stack.push(pivot);
			} else {
				char peek = (stack.peekFirst()==null)?0:stack.peek() ;
				if (peek == 0)
					return "NO";
				if (
						(pivot == ')' && peek == '(') ||
						(pivot == '}' && peek == '{') ||
						(pivot == ']' && peek == '[')) {
					stack.pop();
				} else {
					return "NO";
				}
			}
		}

		if (stack.size() == 0) {
			return "YES";
		} else {
			return "NO";
		}
	}

	@Test
	public void balancedBracketsTest() {
		String test1 = "}][}}(}][))]";
		String test2 = "[](){()}";
		String test3 = "()";
		String test4 = "({}([][]))[]()";
		String test5 = "{)[](}]}]}))}(())(";
		String test6 = "([[)";
		
		String balancedBrackets1 = balancedBrackets(test1);
		assertEquals("NO", balancedBrackets1);
		
		String balancedBrackets2 = balancedBrackets(test2);
		assertEquals("YES", balancedBrackets2);
		
		String balancedBrackets3 = balancedBrackets(test3);
		assertEquals("YES", balancedBrackets3);
		
		String balancedBrackets4 = balancedBrackets(test4);
		assertEquals("YES", balancedBrackets4);
		
		String balancedBrackets5 = balancedBrackets(test5);
		assertEquals("NO", balancedBrackets5);
		
		String balancedBrackets6 = balancedBrackets(test6);
		assertEquals("NO", balancedBrackets6);
	}

}
