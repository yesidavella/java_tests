package codeexercises.com;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Test;

public class Java8_code_exercises {

	public String reverse(String word) {

		StringBuilder reversedWord = new StringBuilder();
//		IntConsumer consumer = (charEle) -> {
//			reversedWord.insert(0, (char) charEle);
//		};
//
//		word.chars().forEach(consumer);
		word.chars().forEach(charEle -> reversedWord.insert(0, (char) charEle));

		return reversedWord.toString();
	}

	public Integer maximum(Integer[] list) throws IllegalArgumentException {

		if (list == null || list.length == 0) {
			throw new IllegalArgumentException("Mariposo no envia ni mierda!!");
		}

		Optional<Integer> max = Arrays.stream(list).max((n1, n2) -> n1 - n2);

		return max.orElseGet(() -> Integer.MAX_VALUE);
	}

	@Test
	public void maximumTest() {

		Integer[] arrayTest1 = new Integer[] { -8, -5, -1, 0, 2, 6, 9 };
		Integer[] arrayTest2 = new Integer[] { 0 };
		Integer[] arrayTest3 = new Integer[] {};
		Integer[] arrayTest4 = new Integer[] { 0, 2, 6, 9 };
		Integer[] arrayTest5 = new Integer[] { -8, -5, -1 };

		int resultTest1 = 9;
		int resultTest2 = 0;
		int resultTest4 = 9;
		int resultTest5 = -1;

		assertTrue(resultTest1 == maximum(arrayTest1), "Fallo en el test1");
		assertTrue(resultTest2 == maximum(arrayTest2), "Fallo en el test2");
		assertTrue(resultTest4 == maximum(arrayTest4), "Fallo en el test4");
		assertTrue(resultTest5 == maximum(arrayTest5), "Fallo en el test5");
		IllegalArgumentException assertThrows2 = assertThrows(IllegalArgumentException.class,
				() -> maximum(arrayTest3));

		assertEquals("Mariposo no envia ni mierda!!", assertThrows2.getMessage());
	}

	public Double average(List<Integer> list) {

		double avg = list.stream().mapToInt(x -> x).average().getAsDouble();

		return avg;
	}

	@Test
	public void averageTest() {

		List<Integer> listTest1 = Arrays.asList(new Integer[] { 1, 2, 3 });

		assertTrue(2 == average(listTest1));
	}

	@Test
	public void reverseTest() {
		String reversedWord = reverse("123456");
//		System.out.println(reversedWord);
		assertEquals("654321", reversedWord);
	}

	public List<String> upperCase(List<String> list) {

		List<String> collectorList = new ArrayList<String>();

		list.stream().map(st -> st.toUpperCase()).forEach(upperString -> collectorList.add(upperString));

		return collectorList;
	}

	@Test
	public void upperCaseTest() {

		List<String> lowerCaseList = new ArrayList<String>();
		lowerCaseList.add("yesid");
		lowerCaseList.add("andres");
		lowerCaseList.add("avella");
		lowerCaseList.add("Ospina");
		lowerCaseList.add("");
		lowerCaseList.add("ultimin");

		List<String> lowerCase = upperCase(lowerCaseList);
		Pattern pattern = Pattern.compile("[A-Z]*");
		boolean allMatch = lowerCase.stream().peek(System.out::println).allMatch(st -> pattern.matcher(st).matches());

		assertTrue(allMatch, "La weaaaa won!!!");
	}

	public List<String> search(List<String> list) {

		List<String> filteredList = new ArrayList<String>();

		list.stream().filter(str -> str.startsWith("a") && str.length() == 3).forEach(fil -> filteredList.add(fil));

		return filteredList;
	}

	@Test
	public void searchTest() {

		List<String> generalList = new ArrayList<String>();
		generalList.add("");
		generalList.add("adf");
		generalList.add("ASA");
		generalList.add("fsdfsdfa");
		generalList.add("adS");
		generalList.add("alv");
		generalList.add("Aaa");
		generalList.add("aBC");

		List<String> filteredList = search(generalList);

		Pattern pattern = Pattern.compile("^a[a-zA-Z]{2}$");
		boolean allMatch = filteredList.stream().allMatch(str -> pattern.matcher(str).matches());

		assertTrue(allMatch, "Algun error filtrando palabras de 3 dig que comienzan con \"a\"");
	}

	public String getString(List<Integer> list) {

		StringBuilder concatResult = new StringBuilder();

		list.stream().forEach(intNum -> {

			if (concatResult.length() != 0)
				concatResult.append(",");

			if (intNum % 2 == 0) {
				concatResult.append("e");
			} else if (intNum % 2 == 1) {
				concatResult.append("o");
			} else {
				concatResult.append("error");
			}
			concatResult.append(intNum);
		});

		return concatResult.toString();
	}

	@Test
	public void getStringTest() {

		List<Integer> integerList = new ArrayList<Integer>();
		integerList.add(1);
		integerList.add(2);
		integerList.add(3);
		integerList.add(4);

		String resultString = getString(integerList);

		boolean allMatch = Arrays.stream(resultString.split(",")).allMatch(

				str -> {
					char trailingChar = str.charAt(0);
					int number = Integer.valueOf(str.substring(1));

					if (trailingChar == 'e' && number % 2 == 0) {
						return true;
					} else if (trailingChar == 'o' && number % 2 == 1) {
						return true;
					} else {
						return false;
					}
				}

		);

		assertTrue(allMatch, "No supo concatenar maricon!!");
	}

	public void streamTest() {

//		IntStream.generate(ThreadLocalRandom.current()::nextInt).limit(5).peek(System.out::println).count();

		Method[] streamMethods = Stream.class.getMethods();

		List<String> collectedList = Arrays.stream(streamMethods).map(method -> method.getName())
				.filter(methodName -> methodName.endsWith("Match"))
//				.peek(System.out::println)
				.sorted((str1, str2) -> str2.compareToIgnoreCase(str1))
//				.peek(System.out::println)
//				.collect(Collectors.toList());
				.collect(Collectors.toCollection(ArrayList<String>::new));

		System.out.println("<<<<<<<<<<<<<<< Total de apariciones >>>>>>>>>>>>>>");
//		finalList.forEach(System.out::println);
//		OptionalDouble optMax = DoubleStream.generate(Math::random).limit(6).max();
//		double val = Double.MIN_NORMAL;
//		optMax.ifPresent( optVal-> System.out.println("Q gono!!:"+optVal) );

		Optional<? extends Object> op = Optional.empty();
//		op = Optional.of("Hola");
//		op = Optional.of(1);

		op.ifPresent(System.out::println);

//		String st = op.map(str -> ((String) str).substring(10)).orElse("Llevados hps!!!");

//		System.out.println("A ver: " + st);

//		IntStream.rangeClosed(1, 5).collect(Math::random, 0, ((x,y)->x*y));
		IntStream.rangeClosed(1, 5).reduce((x, y) -> (x * y)).getAsInt();

		Comparator<String> compareAlphabetically = String::compareTo;

		String sentence = "follow your heart but take your brain with you";
//		Arrays.stream(sentence.split(" ")).distinct().sorted(compareAlphabetically.reversed())
//				.forEach(System.out::println);

		int i = 1;
		Map<String, Integer> map = Stream.of("Arnold", "Alois", "Schwarzenegger")
				.collect(Collectors.toMap(name -> name, name -> name.length()));

//		map.forEach((key,value)->System.out.println("Nombre:"+key+" Valor:"+value));
		Matcher matcher = Pattern.compile(" ").matcher("you never know what you have until you clean your room");

		Stream<String> of = Arrays.stream("you never know what you have until you clean your room".split(" "))
				.distinct();

		Map<Integer, List<String>> collect = of.collect(Collectors.groupingBy(word -> word.length()));

		collect.forEach((value, values) -> {
			System.out.printf("Con tamaño de: %d %n", value);
			values.forEach(System.out::println);
		}
//				(count, words) -> {
//					System.out.printf("word(s) of length %o", count);
//					words.forEach(System.out::println);
//				}
		);

		Arrays.stream("Tu nunca sabes lo que tienes hasta que limpias tu habitacion!!".split(" ")).distinct()
//		.sorted()
//		.peek(System.out::println)
				.map(word -> Arrays.stream(word.split(""))).distinct()
//		.peek(System.out::println)
				.flatMap(chart -> chart).distinct().sorted(Collections.reverseOrder()).peek(System.out::println)
				.count();

	}

	@Test
	public void test() {
		streamTest();
	}

	public void dateJava8() {

		Date aDate = null;
		try {
			aDate = new SimpleDateFormat("yyyy-mm-dd").parse("2012-01-15");
			Calendar aCalendar = Calendar.getInstance();
			aCalendar.setTime(aDate);
			System.out.print(aCalendar.get(aCalendar.DAY_OF_MONTH) + "," + aCalendar.get(aCalendar.MONTH));

			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			LocalDate bDate = LocalDate.parse("2012-01-15", formatter);
			System.out.print(" " + bDate.getDayOfMonth() + "," + bDate.getMonthValue());

		} catch (ParseException ex) {
			System.out.println("ParseException " + ex);
		} catch (DateTimeParseException ex) {
			System.out.println(" DateTimeParseException " + ex);
		}

	}

	@Test
	public void testDateJava8() {
		dateJava8();
	}

	public void dateJava8SecondTest() {

		Date aDate = null;
		Calendar aCalendar = Calendar.getInstance();
		aCalendar.setTimeInMillis(1450000000000L);
		aDate = aCalendar.getTime();
		System.out.print(new SimpleDateFormat("dd-MMM-yyyy").format(aDate));

		Instant anInstant = Instant.ofEpochMilli(1450000000000L);
		DateTimeFormatter d = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		System.out.println(" " + LocalDateTime.ofInstant(anInstant, ZoneId.systemDefault()).format(d));

		aCalendar.add(Calendar.DAY_OF_MONTH, 60);
		aDate = aCalendar.getTime();
		System.out.print(new SimpleDateFormat("dd-MMM-yyyy").format(aDate));

		anInstant.plus(60, ChronoUnit.DAYS);
		System.out.println(" " + LocalDateTime.ofInstant(anInstant, ZoneId.systemDefault()).format(d));
	}

	@Test
	public void testDateJava8SecondTest() {
		dateJava8SecondTest();
	}
	
	class Person {
		String name;
		Integer id;
		Person(String n, Integer i) { name=n; id=i; }
		Person(Integer i) {name=null; id=i;}
		@Override public String toString() { return name + " " + id; }
	}
	

	List<Person> people = Arrays.asList(new Person("Bob", 1),
			new Person(2),
			new Person("Jane", 3));
	
	static int x;
	public Person reducingPeople() {
		
		Person person = people.stream().reduce((e1, e2) -> {
			x = e1.id;
			if (e1.id > e2.id) {
				return e1;
			}
			x = e2.id;
			return e2;
		}).flatMap(e -> Optional.ofNullable(e.name))
		.map(y -> new Person(y, x))
		.orElse(new Person(-1));
		
		return person;
	}

	@Test
	public void testPerson() {
		Person person = reducingPeople();
		assertEquals("Jane 3", person.toString(), "Fallaste en el reduce amigo!");
	}

}
