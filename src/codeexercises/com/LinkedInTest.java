package codeexercises.com;

import java.awt.List;
import java.util.ArrayList;
import java.util.LinkedList;

public class LinkedInTest {

	static LinkedInTest link = new LinkedInTest();

	public static void main(String[] args) {
		System.out.println("A");
		
		try {
			bad();
		} catch (Exception e) {
			System.out.println("B");
			e.printStackTrace();
		} finally{
			System.out.println("C");
		}
	}
	
	public static void bad() {
		throw new Error();
	}

	interface One {
		public default void methodOne() {
			System.out.println("One");
		}
	}

	interface Two {
		public default void methodTwo() {
			System.out.println("Two");
		}
	}

	class Third implements One, Two {

		public void third() {
			methodOne();
			methodTwo();
		}

		// @Override
		// public void method() {
		// System.out.println("Third!!!");
		// }
	}
}