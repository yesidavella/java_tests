package generics;

public class GenericContainer <T> {

	private T root;
	
	public T getRoot() {
		return root;
	}
	
	public void setRoot(T root) {
		this.root = root;
	}

	@Override
	public String toString() {
		return "GenericContainer [root=" + root + "]";
	}
	
	
	
}
