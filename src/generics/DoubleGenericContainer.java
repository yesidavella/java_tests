package generics;

public class DoubleGenericContainer<R, Ll, Rl> {

	private R root;
	private Ll left;
	private Rl right;
	
	public R getRoot() {
		return root;
	}

	public void setRoot(R root) {
		this.root = root;
	}

	public Ll getLeft() {
		return left;
	}

	public void setLeft(Ll left) {
		this.left = left;
	}

	public Rl getRight() {
		return right;
	}

	public void setRight(Rl right) {
		this.right = right;
	}

	@Override
	public String toString() {
		return "DoubleGenericContainer [root=" + root + ", left=" + left + ", right=" + right + "]";
	}

}
