package generics;

class Parent {
	protected static int count = 0;

	public Parent() {
		count++;
	}

	static int getCount() {
		return count;
	}
}

public class Child extends Parent {
	public Child() {
		count++;
	}

	public static void main(String[] args) {
		
		
		String str1 = "My String";
		String str2 = new String ("My String");
		
		System.out.println();
	}
}
